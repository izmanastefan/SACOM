package com.company;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Application {
    ReadOrders orders = new ReadOrders();
    List<String> supplierList = orders.getSupplierContent();
    List<String> descriptionList = orders.getDescriptionContent();
    List<String> priceList = orders.getPriceContent();
    List<String> gtinList = orders.getGtinContent();
    List<String> ordersList = orders.getOrderNumber();

    public Application() throws ParserConfigurationException, TransformerException, SAXException, IOException {
    }
    //create .xml file with speciefied nodes and orders
    public void factoryTransform(int i,Document document) throws ParserConfigurationException, TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new File("C:\\Users\\izman\\Documents\\SACOM\\src\\ProcessedOrders\\" + supplierList.get(i) + getDigits() + ".xml"));
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, result);
    }
    //create element products and product for each order
    public Element createElementProducts(Document document){
        Element element = document.createElement("products");
        document.appendChild(element);
        //product element
        Element product = document.createElement("product");
        element.appendChild(product);
    return  element;
    }
    //create a document
    public Document creatDOC() throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document document = docBuilder.newDocument();
        return document;
    }
    //create node description
    public  Element createElementDescription(int i,Document document,Element product){
            Element description = document.createElement("description");
            description.appendChild(document.createTextNode(descriptionList.get(i)));
            product.appendChild(description);
        return description;
    }
    //create node gtin
    public  Element createElementGtin(int i,Document document,Element product){
            Element gtin = document.createElement("gtin");
            gtin.appendChild(document.createTextNode(gtinList.get(i)));
            product.appendChild(gtin);
        return gtin;
    }
    //create node price
    public  Element createElementPrice(int i,Document document,Element product){
            Element price = document.createElement("price");
            price.appendChild(document.createTextNode(priceList.get(i)));
            product.appendChild(price);
        return price;
    }
    //crate node ordered
    public  Element createElementOrders(int j,Document document,Element product){
            Element orders = document.createElement("ordered");
            orders.appendChild(document.createTextNode(ordersList.get(j)));
            product.appendChild(orders);
        return orders;
    }
    //gets digits from the middle of file orders##.xml
    public String getDigits(){
        File file = new File("src/InputOrders/orders23.xml");
        String nameFile = file.getName();
        String digits = nameFile.substring(6, 8);
        return digits;
    }

    //write in xml file separated orders
    public void writeInXMLFile() throws ParserConfigurationException, TransformerException, IOException, SAXException {

        Document document = creatDOC();
        Element product = createElementProducts(document);


        Document document1 = creatDOC();
        Element product1 = createElementProducts(document1);


        Document document2 = creatDOC();
        Element product2 = createElementProducts(document2);



        for(int i= 0;i<supplierList.size();i++){


          switch (supplierList.get(i)) {
                case "Sony":
                    createElementDescription(i,document,product);
                    createElementGtin(i,document,product);
                    createElementPrice(i,document,product);

                    for(int j=0;j<ordersList.size();j++) {
                        createElementOrders(j, document, product);
                        break;
                    }
                    factoryTransform(i,document);
                    break;
              case "Apple":
                  createElementDescription(i,document1,product1);
                  createElementGtin(i,document1,product1);
                  createElementPrice(i,document1,product1);

                  for(int j=0;j<ordersList.size();j++) {
                      createElementOrders(j, document1, product1);
                      break;
                  }
                  factoryTransform(i,document1);
                  break;


                case "Panasonic":

                    createElementDescription(i,document2,product2);
                    createElementGtin(i,document2,product2);
                    createElementPrice(i,document2,product2);

                    for(int j=0;j<ordersList.size();j++) {
                        createElementOrders(j, document2, product2);
                        break;
                    }
                    factoryTransform(i,document2);
                    break;


            }
        }

    }
}
