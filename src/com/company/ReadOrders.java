package com.company;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadOrders {

    public Document readContent() throws IOException, SAXException, ParserConfigurationException, TransformerException {

        File file = new File("src/InputOrders/orders" + 23 + ".xml");

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);

        return document;
    }

    public List getDescriptionContent() throws IOException, SAXException, ParserConfigurationException, TransformerException {
        NodeList nodeList = readContent().getElementsByTagName("product");
        String description="";
        List list = new ArrayList();
            for (int i = 0; i < nodeList.getLength(); i++) {
                if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    description = readContent().getElementsByTagName("description").item(i).getTextContent();

                    list.add(description);
                }
            }
            return list;
    }

    public List getGtinContent() throws IOException, SAXException, ParserConfigurationException, TransformerException {

        NodeList nodeList = readContent().getElementsByTagName("product");
        String gtin="";
        List list = new ArrayList();
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                gtin = readContent().getElementsByTagName("gtin").item(i).getTextContent();

                list.add(gtin);
            }
        }
        return list;
    }

    public List getPriceContent() throws IOException, SAXException, ParserConfigurationException, TransformerException {
        NodeList nodeList = readContent().getElementsByTagName("product");
        String price="";
        List list = new ArrayList();
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                price = readContent().getElementsByTagName("price").item(i).getTextContent();

                list.add(price);
            }
        }
        return list;
    }

    public List getSupplierContent() throws ParserConfigurationException, SAXException, IOException, TransformerException {
        NodeList nodeList = readContent().getElementsByTagName("product");
        String supplier="";
        List list = new ArrayList();
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                supplier = readContent().getElementsByTagName("supplier").item(i).getTextContent();

                list.add(supplier);
            }
        }
        return list;
    }

    public List getOrderNumber() throws ParserConfigurationException, SAXException, IOException, TransformerException {
        NodeList nodeList = readContent().getElementsByTagName("order");
        String number="";
        List list = new ArrayList();
        for (int i = 0; i < nodeList.getLength(); i++) {

                Node node = nodeList.item(i);
                Element element = (Element) node;
               number= element.getAttribute("ID");
                list.add(number);
            }
              return list;
    }

}
